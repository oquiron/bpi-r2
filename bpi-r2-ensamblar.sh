# Ensamblado de Debian Buster para Banana PI R2
# Quiron
# 2022-09-15 Funciona


# El sitio oficial de Banana PI R2 es:
# https://github.com/BPI-SINOVOIP/BPI-R2-bsp
# Contiene u-boot y la imagen del núcleo 4.4

## Particionado
# https://www.fw-web.de/dokuwiki/doku.php?id=en:bpi-r2:storage

# BootLoader:
#	0-2k		Header
#	2k-220k		Preloader
#	320k-1M		UBoot
#	1M-100M		Reservado
# Datos:
#	100MB-356MB	FAT23 BPI-BOOT
#	256MB-7456MB	EXT4  BPI-ROOT


# Crear en disco una imagen SD vacia
# instrucciones tomadas de:
# https://www.fw-web.de/dokuwiki/doku.php?id=en:bpi-r2:storage#step-by-step-guide_from_ul90

apt install -y  microcom mmc-utils dosfstools

function pausa(){
	echo "Pulsa una tecla para continuar ..."
	read x
}
set -e

distro=bullseye
echo "Crear la imagen"
#dd if=/dev/zero of=bpi-r2-${distro}.img bs=1M count=7168
rm bpi-r2-${distro}.img

fallocate -l 8G bpi-r2-${distro}.img
DEV=$(losetup -f)
echo $DEV libre
#losetup -d ${DEV}  || true
losetup ${DEV} bpi-r2-${distro}.img
parted -s ${DEV} mklabel msdos
parted -s ${DEV} unit MiB mkpart primary fat32 -- 100MiB 356MiB
parted -s ${DEV} unit MiB mkpart primary ext4 -- 356MiB  7516MB #7295MiB
partprobe ${DEV}
mkfs.vfat ${DEV}p1 -I -n BPI-BOOT
mkfs.ext4 -O ^has_journal -E stride=2,stripe-width=1024 -b 4096 ${DEV}p2 -L BPI-ROOT
sync
parted -s ${DEV} print
pausa
# Instalación de Debian en BananaPi R2

# ==================================================================
#  BootLoader
#curl 'https://www.fw-web.de/dokuwiki/lib/exe/fetch.php?media=bpi-r2:bpi-r2-head440-0k.img'
#curl 'https://www.fw-web.de/dokuwiki/lib/exe/fetch.php?media=bpi-r2:bpi-r2-head1-512b.img'

# Imagenes oficiales
#  https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/ 
# ------

if [ "$1" == "descargar" ] ; then

	echo "Descargarndo "

	curl -O -L https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/100MB/BPI-R2-HEAD440-0k.img.gz
	curl -O -L https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/100MB/BPI-R2-HEAD1-512b.img.gz
	# ------
	curl -O -L https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/100MB/BPI-R2-preloader-DDR1600-20190722-2k.img.gz
	# curl https://github.com/BPI-SINOVOIP/BPI-files/blob/master/SD/100MB/BPI-R2-preloader-DDR1600-20191024-2k.img.gz

	# Y esta otra más, ( es la del tutorioal ul90 ) https://www.fw-web.de/dokuwiki/doku.php?id=en:bpi-r2:storage
	# https://github.com/BPI-SINOVOIP/BPI-R2-bsp/tree/master/mt-pack/mtk/bpi-r2/bin
	# curl https://github.com/BPI-SINOVOIP/BPI-R2-bsp/blob/master/mt-pack/mtk/bpi-r2/bin/preloader_iotg7623Np1_emmc.bin

	# ------
	#curl -O -L https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/100MB/u-boot-2019.07-bpi-r2-2k.img.gz
	# También aquí esta un u-boot, no se cuál es la diferencia
	# curl https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/BPI-ROOT/BOOTLOADER-bpi-r2.tgz
	# vamos a cambiar de imagen uboot
	curl -O -l https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/100MB/BPI-R2-720P-2k.img.gz


fi
gunzip -c BPI-R2-HEAD440-0k.img.gz | dd of=${DEV} seek=0 bs=1024  # creo que debería ser bs=512
gunzip -c BPI-R2-HEAD1-512b.img.gz | dd of=${DEV} seek=1 bs=512 



# Write preloader and u-boot bootloader:

echo "Grabando BootLoader"
gunzip -c BPI-R2-preloader-DDR1600-20190722-2k.img.gz | dd of=${DEV} seek=2   bs=1024 
#gunzip -c u-boot-2019.07-bpi-r2-2k.img.gz             | dd of=${DEV} seek=320 bs=1024  ## Aquí hay algo raro: ¿Porqué dice 2k?
#gunzip -c BPI-R2-720P-2k.img.gz             | dd of=${DEV} seek=320 bs=1024  ## Aquí hay algo raro: ¿Porqué dice 2k?
# Al parecer ... por eso decía 2k... 
# http://www.fw-web.de/dokuwiki/doku.php?id=en:bpi-r2:uboot
gunzip -c BPI-R2-720P-2k.img.gz             | dd of=${DEV} bs=1k seek=2 count=1022

#dd if=bsp14/mt-pack/mtk/bpi-r2/bin/preloader_iotg7623Np1_sd_1600M.bin of=bpi-r2-buster.img  bs=1024 seek=2

#dd if= of=bpi-r2-buster.img  bs=1024 seek=320


sync
pausa
# verisiones actualizadas y oficiales de 
## https://github.com/BPI-SINOVOIP/BPI-files/tree/master/SD/100MB
 


# ===================================================================
# kernel
# Versiones oficiales del kernel 4.4
# https://github.com/BPI-SINOVOIP/BPI-files/tree/master/SD/BPI-ROOT

if [ "$1" == "descargar" ] ; then

	# Imagen oficial del kernel 4.4 ( BPI-ROOT )
	# curl -O -L https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/BPI-BOOT/BPI-BOOT-bpi-r2.tgz
	## No se cuál es la diferencia al -net.tgz
	## curl https://github.com/BPI-SINOVOIP/BPI-files/blob/master/SD/BPI-ROOT/4.4.70-BPI-R2-Kernel-net.tgz

	# Módulos
	# curl -O -L https://github.com/BPI-SINOVOIP/BPI-files/raw/master/SD/BPI-ROOT/4.4.70-BPI-R2-Kernel.tgz
	## curl https://github.com/BPI-SINOVOIP/BPI-files/blob/master/SD/BPI-ROOT/4.4.70-BPI-R2-Kernel-net.tgz


	# El usuario frank-w mantiene el kernel para bpi-r2
	# El pr	oyecto se llama "Kernel 4.14 para BPI-R2" pero contienen versiones 5 del kernel.
	# https://github.com/frank-w/BPI-R2-4.14
#	curl -O -L https://github.com/frank-w/BPI-R2-4.14/releases/download/CI-BUILD-5.15-main-5.15.63-2022-08-28_1320/bpi-r2_5.15.63-main.tar.gz
#	curl -O -L https://github.com/frank-w/BPI-R2-4.14/releases/download/CI-BUILD-5.15-main-5.15.63-2022-08-28_1320/bpi-r2_5.15.63-main.tar.gz.md5

	curl -O -L https://github.com/frank-w/BPI-Router-Linux/releases/download/CI-BUILD-6.1-main-6.1.0-2023-01-18_1758/bpi-r2_6.1.0-main.tar.gz
	curl -O -L https://github.com/frank-w/BPI-Router-Linux/releases/download/CI-BUILD-6.1-main-6.1.0-2023-01-18_1758/bpi-r2_6.1.0-main.tar.gz.md5

	# The linux-kernel has 2 parts…the “main-kernel-image” (uImage) and the modules.
	# The kernel-image is loaded by uboot from BPI-BOOT-Partition folder bananapi/bpi-r2/linux/. By default the file “uImage” is loaded, but you can choose another name and set variable “kernel” in uEnv.txt.
	# the modules have to be put to BPI-ROOT-partition (/lib/modules/kernelname).




fi

echo "Copiando el kernel"

[ -d /tmp/BPI-BOOT ] ||  mkdir /tmp/BPI-BOOT
mount ${DEV}p1 /tmp/BPI-BOOT


[ -d /tmp/BPI-ROOT ] || mkdir /tmp/BPI-ROOT
mount ${DEV}p2 /tmp/BPI-ROOT


# tar -xf BPI-BOOT-bpi-r2.tgz -C /tmp/BPI-BOOT
# tar -xf 4.4.70-BPI-R2-Kernel.tgz -C /tmp/BPI-ROOT
#tar -xf bpi-r2_5.15.63-main.tar.gz -C /tmp
tar -xf bpi-r2_6.1.0-main.tar.gz -C /tmp
find  /tmp/BPI-*
df -h
pausa

# Error:
# tar: lib/modules/4.4.70-BPI-R2-Kernel/source: Cannot create symlink to ‘/root/Code/release_projects/BPI-R2/BPI-R2-bsp/linux-mt’: Operation not permitted
# Supongo que se puede omitir, pues creo que no se necesitará el código fuente




# ===================================================================
# Debian rootfs
function debian(){
echo "Creando imagen Debian con debootstrap"
# binfmt-support permite el chroot cros-arquitecturas
apt -y install qemu-user-static debootstrap binfmt-support
#distro=bullseye
arch=armhf
#targetdir=$(pwd)/debian_${distro}_${arch}
targetdir=/tmp/BPI-ROOT/
debootstrap --arch=$arch --foreign $distro $targetdir
cp /usr/bin/qemu-arm-static $targetdir/usr/bin/
cp /etc/resolv.conf $targetdir/etc

echo "Ingresando chroot"
cat << EOF | distro=$distro chroot $targetdir
export LANG=C
/debootstrap/debootstrap --second-stage
apt install -y openssh-server
sed -i 's/#PermitRootLogin/PermitRootLogin yes #/' /etc/ssh/sshd_config
echo root:123456 | chpasswd 
apt clean


# no debe haber TABs
echo "
/dev/mmcblk0p1      /boot       vfat    errors=remount-ro   0       1
/dev/mmcblk0p2      /           ext4    defaults	    0       0
" >> /etc/fstab

echo "
auto eth0
iface eth0 inet manual
  pre-up ip link set $IFACE up
  post-down ip link set $IFACE down

auto lan0
iface lan0 inet static
  hwaddress ether 08:00:00:00:00:00 # if you want to set MAC manually
  address 192.168.0.10
  netmask 255.255.255.0
  gateway 192.168.0.5
  pre-up ip link set $IFACE up
  post-down ip link set $IFACE down
" >> /etc/network/interfaces


echo "deb http://deb.debian.org/debian buster main contrib non-free" > /etc/apt/sources.list

echo "bpi-r2 > /etc/hostname
echo "172.0.1.1 bpi-r2 >> /etc/hosts



EOF


}


echo "Desmontando "
# desmonta la imagen
umount /tmp/BPI-BOOT /tmp/BPI-ROOT
sleep 4
ls /tmp/BPI-*

losetup -d ${DEV}

# Crear SD
# https://wiki.banana-pi.org/Getting_Start_with_R2
# https://wiki.banana-pi.org/Getting_Start_with_R2#Load_your_first_image_on_R2


# No funciona con conv=sparse
echo "dd  status=progress  if=bpi-r2-buster.img of=/dev/el_que_corresponda"



